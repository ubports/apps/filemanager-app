Overview of changes in lomiri-filemanager-app 1.1.3

  - Update files count on inserting files.
  - Fix binding loops in FolderListPageSelectionHeader.
  - Translation updates (thanks to all contributors on
    hosted.weblate.org).

Overview of changes in lomiri-filemanager-app 1.1.2

  - CMakeLists.txt: Let desktop icon be found via lookup in non-click
    mode.
  - CMakeLists.txt: Also install content-hub & url-dispatcher files in
    non-click mode.
  - .gitlab-ci.yml: use shared runner if run outside our namespace.

Overview of changes in lomiri-filemanager-app 1.1.1

  - .gitlab-ci.yml: Use UBports GitLab runner.

Overview of changes in lomiri-filemanager-app 1.1.0

  - Snap support: Change QT_QPA_PLATFORM environment variable check to
    "contains" rather than expecting exactness.
  - Add url-dispatcher support.
  - Open new view in file picker mode and exit properly.
  - CMake: Split CMAKE_CXX_FLAGS assignment into purpose-specific
    variables/calls.
  - CMake: Use find_package REQUIRE for Gettext.
  - Get smbclient flags via pkg-config, if possible.
  - CMake: Stop using qt5_use_modules.
  - Include paths.h.
  - Call i18n.bindtextdomain with buildtime-determined locale path.
  - Set gettext domain correctly, use correct gettext domain.
  - Translation updates (thanks to all contributors on
    hosted.weblate.org).

Overview of changes in lomiri-filemanager-app 1.0.4

  - Fix incorrect initialization of PathHistoryRow (@AlTeveDev).
  - Translation updates.
